import unittest

from scientpyfic.client import ScientPyClient

class TestComplex(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_climate(self):
    result = self.client.environment.earth_climate.earth_science.atmosphere.climate(title=False, body=True, journals=True, latest=1)
    
    self.assertTrue(len(result) > 0 and result[0].title is None and
                     result[0].body is not None and result[0].journals is not None)
