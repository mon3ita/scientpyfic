import unittest

from scientpyfic.client import ScientPyClient

class TestFossilsRuins(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_fossils_ruins(self):
    result = self.client.environment.fossils_ruins.fossils_and_ruins()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
