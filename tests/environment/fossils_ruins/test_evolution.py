import unittest

from scientpyfic.client import ScientPyClient

class TestEvolution(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_ancient_dna(self):
    result = self.client.environment.fossils_ruins.evolution.ancient_dna()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_human_evolution(self):
    result = self.client.environment.fossils_ruins.evolution.human_evolution()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_origin_of_life(self):
    result = self.client.environment.fossils_ruins.evolution.origin_of_life()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_charles_darwin(self):
    result = self.client.environment.fossils_ruins.evolution.charles_darwin()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_evolution(self):
    result = self.client.environment.fossils_ruins.evolution.evolution()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
