import unittest

from scientpyfic.client import ScientPyClient

class TestAnthropology(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_anthtopology(self):
    result = self.client.environment.fossils_ruins.anthropology.anthropology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cultures(self):
    result = self.client.environment.fossils_ruins.anthropology.cultures()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_early_humans(self):
    result = self.client.environment.fossils_ruins.anthropology.early_humans()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
