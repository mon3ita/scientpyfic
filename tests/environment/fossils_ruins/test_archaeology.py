import unittest

from scientpyfic.client import ScientPyClient

class TestArchaeology(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_lost_treasures(self):
    result = self.client.environment.fossils_ruins.archaeology.lost_treasures()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_archaeology(self):
    result = self.client.environment.fossils_ruins.archaeology.archaeology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_evolution(self):
    result = self.client.environment.fossils_ruins.archaeology.evolution()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ancient_civilizations(self):
    result = self.client.environment.fossils_ruins.archaeology.ancient_civilizations()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)