import unittest

from scientpyfic.client import ScientPyClient

class TestPaleontology(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_early_birds(self):
    result = self.client.environment.fossils_ruins.paleontology.early_birds()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_dinosaurs(self):
    result = self.client.environment.fossils_ruins.paleontology.dinosaurs()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_paleontology(self):
    result = self.client.environment.fossils_ruins.paleontology.paleontology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_early_climate(self):
    result = self.client.environment.fossils_ruins.paleontology.early_climate()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_early_mammals(self):
    result = self.client.environment.fossils_ruins.paleontology.early_mammals()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_tyrannosaurus_rex(self):
    result = self.client.environment.fossils_ruins.paleontology.tyrannosaurus_rex()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fossils(self):
    result = self.client.environment.fossils_ruins.paleontology.fossils()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)