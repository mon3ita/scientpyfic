import unittest

from scientpyfic.client import ScientPyClient

class TestMiscorbesMore(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_microbes_and_more(self):
    result = self.client.environment.plants_animals.microbes_more.microbes_and_more()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_microbiology(self):
    result = self.client.environment.plants_animals.microbes_more.microbiology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_virology(self):
    result = self.client.environment.plants_animals.microbes_more.virology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_bacteria(self):
    result = self.client.environment.plants_animals.microbes_more.bacteria()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fungus(self):
    result = self.client.environment.plants_animals.microbes_more.fungus()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_prions(self):
    result = self.client.environment.plants_animals.microbes_more.prions()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
