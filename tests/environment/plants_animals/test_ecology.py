import unittest

from scientpyfic.client import ScientPyClient

class TestEcology(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_extinction(self):
    result = self.client.environment.plants_animals.ecology.extinction()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_trees(self):
    result = self.client.environment.plants_animals.ecology.trees()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_wild_animals(self):
    result = self.client.environment.plants_animals.ecology.wild_animals()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_endangered_plants(self):
    result = self.client.environment.plants_animals.ecology.endangered_plants()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_extreme_survival(self):
    result = self.client.environment.plants_animals.ecology.extreme_survival()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_nature(self):
    result = self.client.environment.plants_animals.ecology.nature()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sea_life(self):
    result = self.client.environment.plants_animals.ecology.sea_life()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_invasive_species(self):
    result = self.client.environment.plants_animals.ecology.invasive_species()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
