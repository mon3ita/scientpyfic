import unittest

from scientpyfic.client import ScientPyClient

class TestEducationLearning(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_animal_learning_and_intelligence(self):
    result = self.client.environment.plants_animals.education_learning.animal_learning_and_intelligence()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
