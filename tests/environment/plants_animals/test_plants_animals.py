import unittest

from scientpyfic.client import ScientPyClient

class TestMiscorbesMore(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_plants_animals(self):
    result = self.client.environment.plants_animals.planst_animals()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)