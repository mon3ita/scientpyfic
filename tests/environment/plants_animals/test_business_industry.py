import unittest

from scientpyfic.client import ScientPyClient

class TestBusinessIndustry(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_biotechnology_and_bioengineering(self):
    result = self.client.environment.plants_animals.business_industry.biotechnology_and_bioengineering()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_food_and_agriculture(self):
    result = self.client.environment.plants_animals.business_industry.food_and_agriculture()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
