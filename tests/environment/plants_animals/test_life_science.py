import unittest

from scientpyfic.client import ScientPyClient

class TestLifeScience(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_biology(self):
    result = self.client.environment.plants_animals.life_science.biology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mating_and_breeding(self):
    result = self.client.environment.plants_animals.life_science.mating_and_breeding()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_developmental_biology(self):
    result = self.client.environment.plants_animals.life_science.developmental_biology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_zoology(self):
    result = self.client.environment.plants_animals.life_science.zoology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_life_sciences(self):
    result = self.client.environment.plants_animals.life_science.life_sciences()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_behavioral_science(self):
    result = self.client.environment.plants_animals.life_science.behavioral_science()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_biochemistry_research(self):
    result = self.client.environment.plants_animals.life_science.biochemistry_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_biotechnology(self):
    result = self.client.environment.plants_animals.life_science.biotechnology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cell_biology(self):
    result = self.client.environment.plants_animals.life_science.cell_biology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_genetics(self):
    result = self.client.environment.plants_animals.life_science.genetics()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_evolutionary_biology(self):
    result = self.client.environment.plants_animals.life_science.evolutionary_biology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_epigenetics_research(self):
    result = self.client.environment.plants_animals.life_science.epigenetics_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_molecular_biology(self):
    result = self.client.environment.plants_animals.life_science.molecular_biology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_marine_biology(self):
    result = self.client.environment.plants_animals.life_science.marine_biology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_botany(self):
    result = self.client.environment.plants_animals.life_science.botany()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
