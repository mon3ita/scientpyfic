import unittest

from scientpyfic.client import ScientPyClient

class TestAnimals(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_cows_sheep_pigs(self):
    result = self.client.environment.plants_animals.animals.cows_sheep_pigs()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_rodents(self):
    result = self.client.environment.plants_animals.animals.rodents()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_veterinary_medicine(self):
    result = self.client.environment.plants_animals.animals.veterinary_medicine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_dogs(self):
    result = self.client.environment.plants_animals.animals.dogs()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mice(self):
    result = self.client.environment.plants_animals.animals.mice()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cats(self):
    result = self.client.environment.plants_animals.animals.cats()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_horses(self):
    result = self.client.environment.plants_animals.animals.horses()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_spiders_and_ticks(self):
    result = self.client.environment.plants_animals.animals.spiders_and_ticks()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_birds(self):
    result = self.client.environment.plants_animals.animals.birds()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_frogs_and_reptiles(self):
    result = self.client.environment.plants_animals.animals.frogs_and_reptiles()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_apes(self):
    result = self.client.environment.plants_animals.animals.apes()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_monkeys(self):
    result = self.client.environment.plants_animals.animals.monkeys()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_new_species(self):
    result = self.client.environment.plants_animals.animals.new_species()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fish(self):
    result = self.client.environment.plants_animals.animals.fish()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_insects(self):
    result = self.client.environment.plants_animals.animals.insects()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_dolphins_and_whales(self):
    result = self.client.environment.plants_animals.animals.dolphins_and_whales()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_animals(self):
    result = self.client.environment.plants_animals.animals.animals()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mammals(self):
    result = self.client.environment.plants_animals.animals.mammals()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
