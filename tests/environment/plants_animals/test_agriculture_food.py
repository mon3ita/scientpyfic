import unittest

from scientpyfic.client import ScientPyClient

class TestAgricultureFood(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_fisheries(self):
    result = self.client.environment.plants_animals.agriculture_food.fisheries()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_genetically_modified(self):
    result = self.client.environment.plants_animals.agriculture_food.genetically_modified()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_pests_and_parasites(self):
    result = self.client.environment.plants_animals.agriculture_food.pests_and_parasites()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cloning(self):
    result = self.client.environment.plants_animals.agriculture_food.cloning()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_drought(self):
    result = self.client.environment.plants_animals.agriculture_food.drought()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_beer_and_wine(self):
    result = self.client.environment.plants_animals.agriculture_food.beer_and_wine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_soil_types(self):
    result = self.client.environment.plants_animals.agriculture_food.soil_types()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_bird_flu_research(self):
    result = self.client.environment.plants_animals.agriculture_food.bird_flu_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_agriculture_and_food(self):
    result = self.client.environment.plants_animals.agriculture_food.agriculture_and_food()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_seeds(self):
    result = self.client.environment.plants_animals.agriculture_food.seeds()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_food(self):
    result = self.client.environment.plants_animals.agriculture_food.food()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_organic(self):
    result = self.client.environment.plants_animals.agriculture_food.organic()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
