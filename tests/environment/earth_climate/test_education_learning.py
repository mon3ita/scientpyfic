import unittest

from scientpyfic.client import ScientPyClient

class TestEducationLearning(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_environmental_awareness(self):
    result = self.client.environment.earth_climate.education_learning.environmental_awareness()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

