import unittest

from scientpyfic.client import ScientPyClient

class TestNaturalDisasters(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_natural_disasters(self):
    result = self.client.environment.earth_climate.natural_disasters.natural_disasters()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_volcanoes(self):
    result = self.client.environment.earth_climate.natural_disasters.volcanoes()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_snow_and_avalanches(self):
    result = self.client.environment.earth_climate.natural_disasters.snow_and_avalanches()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_tsunamis(self):
    result = self.client.environment.earth_climate.natural_disasters.tsunamis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_geomagnetic_storms(self):
    result = self.client.environment.earth_climate.natural_disasters.geomagnetic_storms()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_landslides(self):
    result = self.client.environment.earth_climate.natural_disasters.landslides()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_earthquakes(self):
    result = self.client.environment.earth_climate.natural_disasters.earthquakes()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_near_earth_object_impacts(self):
    result = self.client.environment.earth_climate.natural_disasters.near_earth_object_impacts()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)