import unittest

from scientpyfic.client import ScientPyClient

class TestAtmosphere(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_climate(self):
    result = self.client.environment.earth_climate.earth_science.atmosphere.climate()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_weather(self):
    result = self.client.environment.earth_climate.earth_science.atmosphere.weather()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_atmosphere(self):
    result = self.client.environment.earth_climate.earth_science.atmosphere.atmosphere()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
