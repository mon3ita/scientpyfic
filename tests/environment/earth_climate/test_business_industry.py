import unittest

from scientpyfic.client import ScientPyClient

class TestBusinessIndustry(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_renewable_energy(self):
    result = self.client.environment.earth_climate.business_industry.renewable_energy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_geoengineering(self):
    result = self.client.environment.earth_climate.business_industry.geoengineering()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_recycling_and_waste(self):
    result = self.client.environment.earth_climate.business_industry.recycling_and_waste()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_industry_mining(self):
    result = self.client.environment.earth_climate.business_industry.mining()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
