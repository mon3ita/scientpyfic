import unittest

from scientpyfic.client import ScientPyClient

class TestEnvironmentalIssues(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_drought_research(self):
    result = self.client.environment.earth_climate.environmental_issues.drought_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_air_pollution(self):
    result = self.client.environment.earth_climate.environmental_issues.air_pollution()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_environmental_policy(self):
    result = self.client.environment.earth_climate.environmental_issues.environmental_policy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_earth_climate_environmental_issues_ice_ages(self):
    result = self.client.environment.earth_climate.environmental_issues.ice_ages()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_oil_spills(self):
    result = self.client.environment.earth_climate.environmental_issues.oil_spills()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_acid_rain(self):
    result = self.client.environment.earth_climate.environmental_issues.acid_rain()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_global_warming(self):
    result = self.client.environment.earth_climate.environmental_issues.global_warming()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ozone_holes(self):
    result = self.client.environment.earth_climate.environmental_issues.ozone_holes()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_drought_pollutin(self):
    result = self.client.environment.earth_climate.environmental_issues.pollution()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_hazardous_waste(self):
    result = self.client.environment.earth_climate.environmental_issues.hazardous_waste()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_air_quality(self):
    result = self.client.environment.earth_climate.environmental_issues.air_quality()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_environmental_issues(self):
    result = self.client.environment.earth_climate.environmental_issues.environmental_issues()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sustainability(self):
    result = self.client.environment.earth_climate.environmental_issues.sustainability()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
