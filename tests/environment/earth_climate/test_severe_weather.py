import unittest

from scientpyfic.client import ScientPyClient

class TestSevereWeather(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_storms(self):
    result = self.client.environment.earth_climate.natural_disasters.severe_weather.storms()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_floods(self):
    result = self.client.environment.earth_climate.natural_disasters.severe_weather.floods()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_el_nino(self):
    result = self.client.environment.earth_climate.natural_disasters.severe_weather.el_nino_and_la_nina()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
  
  def test_severe_weather(self):
    result = self.client.environment.earth_climate.natural_disasters.severe_weather.severe_weather()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
