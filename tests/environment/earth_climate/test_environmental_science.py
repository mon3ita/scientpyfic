import unittest

from scientpyfic.client import ScientPyClient

class TestEnvironmentalScience(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_energy_and_the_environment(self):
    result = self.client.environment.earth_climate.environmental_science.energy_and_the_environment()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_desert(self):
    result = self.client.environment.earth_climate.environmental_science.desert()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ecology(self):
    result = self.client.environment.earth_climate.environmental_science.ecology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_water(self):
    result = self.client.environment.earth_climate.environmental_science.water()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_coral_reefs(self):
    result = self.client.environment.earth_climate.environmental_science.coral_reefs()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_rainforests(self):
    result = self.client.environment.earth_climate.environmental_science.rainforests()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_wildfires(self):
    result = self.client.environment.earth_climate.environmental_science.wildfires()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_caving(self):
    result = self.client.environment.earth_climate.environmental_science.caving()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ecosystems(self):
    result = self.client.environment.earth_climate.environmental_science.ecosystems()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_environmental_science(self):
    result = self.client.environment.earth_climate.environmental_science.environmental_science()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_biodiversity(self):
    result = self.client.environment.earth_climate.environmental_science.biodiversity()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_tundra(self):
    result = self.client.environment.earth_climate.environmental_science.tundra()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_exotic_species(self):
    result = self.client.environment.earth_climate.environmental_science.exotic_species()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_forest(self):
    result = self.client.environment.earth_climate.environmental_science.forest()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_grassland(self):
    result = self.client.environment.earth_climate.environmental_science.grassland()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
