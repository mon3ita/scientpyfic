import unittest

from scientpyfic.client import ScientPyClient

class TestEarthScience(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_geography(self):
    result = self.client.environment.earth_climate.earth_science.geography()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_earth_science(self):
    result = self.client.environment.earth_climate.earth_science.earth_science()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_geochemistry(self):
    result = self.client.environment.earth_climate.earth_science.geochemistry()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_geology(self):
    result = self.client.environment.earth_climate.earth_science.geology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_oceanography(self):
    result = self.client.environment.earth_climate.earth_science.oceanography()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
