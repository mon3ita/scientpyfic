import unittest

from scientpyfic.client import ScientPyClient

class TestLivingWell(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_healthy_aging(self):
    result = self.client.health.health_medicine.living_well.healthy_aging()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mens_health(self):
    result = self.client.health.health_medicine.living_well.mens_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_nutrition(self):
    result = self.client.health.health_medicine.living_well.nutrition()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_teen_health(self):
    result = self.client.health.health_medicine.living_well.teen_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_pregnancy_and_childbirth(self):
    result = self.client.health.health_medicine.living_well.pregnancy_and_childbirth()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_elder_care(self):
    result = self.client.health.health_medicine.living_well.elder_care()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fitness(self):
    result = self.client.health.health_medicine.living_well.fitness()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_womens_health(self):
    result = self.client.health.health_medicine.living_well.womens_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_skin_care(self):
    result = self.client.health.health_medicine.living_well.skin_care()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_todays_healthcare(self):
    result = self.client.health.health_medicine.living_well.todays_healthcare()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_breastfeeding(self):
    result = self.client.health.health_medicine.living_well.breastfeeding()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sports_medicine(self):
    result = self.client.health.health_medicine.living_well.sports_medicine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sexual_health(self):
    result = self.client.health.health_medicine.living_well.sexual_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fertility(self):
    result = self.client.health.health_medicine.living_well.fertility()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cosmetic_surgery(self):
    result = self.client.health.health_medicine.living_well.cosmetic_surgery()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_vegetarian(self):
    result = self.client.health.health_medicine.living_well.vegetarian()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_staying_healthy(self):
    result = self.client.health.health_medicine.living_well.staying_healthy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_childrens_health(self):
    result = self.client.health.health_medicine.living_well.childrens_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_infnants_health(self):
    result = self.client.health.health_medicine.living_well.infnants_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_diet_and_weight_loss(self):
    result = self.client.health.health_medicine.living_well.diet_and_weight_loss()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
