import unittest

from scientpyfic.client import ScientPyClient

class TestDiseasesConditionals(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_bladder_disorders(self):
    result = self.client.health.health_medicine.diseases_conditionals.bladder_disorders()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_copd(self):
    result = self.client.health.health_medicine.diseases_conditionals.copd()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ebola(self):
    result = self.client.health.health_medicine.diseases_conditionals.ebola()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_irritable_bowel_syndrome(self):
    result = self.client.health.health_medicine.diseases_conditionals.irritable_bowel_syndrome()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_restless_leg_syndrome(self):
    result = self.client.health.health_medicine.diseases_conditionals.restless_leg_syndrome()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cerebral_palsy(self):
    result = self.client.health.health_medicine.diseases_conditionals.cerebral_palsy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_malaria(self):
    result = self.client.health.health_medicine.diseases_conditionals.malaria()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_prostate_health(self):
    result = self.client.health.health_medicine.diseases_conditionals.prostate_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_birth_defects(self):
    result = self.client.health.health_medicine.diseases_conditionals.birth_defects()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_eating_disorder_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.eating_disorder_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_infectious_diseases(self):
    result = self.client.health.health_medicine.diseases_conditionals.infectious_diseases()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_alzheimers_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.alzheimers_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_allergy(self):
    result = self.client.health.health_medicine.diseases_conditionals.allergy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_epilepsy_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.epilepsy_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fibromyalgia(self):
    result = self.client.health.health_medicine.diseases_conditionals.fibromyalgia()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_gastrointestinal_problems(self):
    result = self.client.health.health_medicine.diseases_conditionals.gastrointestinal_problems()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mumps_measles_rubella(self):
    result = self.client.health.health_medicine.diseases_conditionals.mumps_measles_rubella()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_obesity(self):
    result = self.client.health.health_medicine.diseases_conditionals.obesity()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_thyroid_disease(self):
    result = self.client.health.health_medicine.diseases_conditionals.thyroid_disease()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_triglycerides(self):
    result = self.client.health.health_medicine.diseases_conditionals.triglycerides()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_tuberculosis(self):
    result = self.client.health.health_medicine.diseases_conditionals.tuberculosis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_insomnia_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.insomnia_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_hormone_disorders(self):
    result = self.client.health.health_medicine.diseases_conditionals.hormone_disorders()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_joint_pain(self):
    result = self.client.health.health_medicine.diseases_conditionals.joint_pain()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ulcers(self):
    result = self.client.health.health_medicine.diseases_conditionals.ulcers()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_colitis(self):
    result = self.client.health.health_medicine.diseases_conditionals.colitis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cystic_fibrosis(self):
    result = self.client.health.health_medicine.diseases_conditionals.cystic_fibrosis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_asthma(self):
    result = self.client.health.health_medicine.diseases_conditionals.asthma()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_erectile_dysfunction(self):
    result = self.client.health.health_medicine.diseases_conditionals.erectile_dysfunction()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_lyme_disease(self):
    result = self.client.health.health_medicine.diseases_conditionals.lyme_disease()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_multiple_sclerosis_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.multiple_sclerosis_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mental_health_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.mental_health_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_urology(self):
    result = self.client.health.health_medicine.diseases_conditionals.urology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_lung_disease(self):
    result = self.client.health.health_medicine.diseases_conditionals.lung_disease()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_crohns_disease(self):
    result = self.client.health.health_medicine.diseases_conditionals.crohns_disease()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_parkinsons_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.parkinsons_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_attention_deficit_disorder(self):
    result = self.client.health.health_medicine.diseases_conditionals.attention_deficit_disorder()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
    
  def test_down_syndrome(self):
    result = self.client.health.health_medicine.diseases_conditionals.down_syndrome()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_hiv_and_aids(self):
    result = self.client.health.health_medicine.diseases_conditionals.hiv_and_aids()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_hair_loss(self):
    result = self.client.health.health_medicine.diseases_conditionals.hair_loss()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_herpes(self):
    result = self.client.health.health_medicine.diseases_conditionals.herpes()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_anemia(self):
    result = self.client.health.health_medicine.diseases_conditionals.anemia()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_arthritis(self):
    result = self.client.health.health_medicine.diseases_conditionals.arthritis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_blood_clots(self):
    result = self.client.health.health_medicine.diseases_conditionals.blood_clots()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_chronic_fatigue_syndrome(self):
    result = self.client.health.health_medicine.diseases_conditionals.chronic_fatigue_syndrome()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_lupus(self):
    result = self.client.health.health_medicine.diseases_conditionals.lupus()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_muscular_dystrophy(self):
    result = self.client.health.health_medicine.diseases_conditionals.muscular_dystrophy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_psoriasis(self):
    result = self.client.health.health_medicine.diseases_conditionals.psoriasis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sleep_disorder_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.sleep_disorder_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_headache_research(self):
    result = self.client.health.health_medicine.diseases_conditionals.headache_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_hypertension(self):
    result = self.client.health.health_medicine.diseases_conditionals.hypertension()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_neuropathy(self):
    result = self.client.health.health_medicine.diseases_conditionals.neuropathy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_liver_disease(self):
    result = self.client.health.health_medicine.diseases_conditionals.liver_disease()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sickle_cell_anemia(self):
    result = self.client.health.health_medicine.diseases_conditionals.sickle_cell_anemia()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_amyotrophic_lateral_sclerosis(self):
    result = self.client.health.health_medicine.diseases_conditionals.amyotrophic_lateral_sclerosis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_diabetes(self):
    result = self.client.health.health_medicine.diseases_conditionals.diabetes()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_hearing_loss(self):
    result = self.client.health.health_medicine.diseases_conditionals.hearing_loss()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_osteoporosis(self):
    result = self.client.health.health_medicine.diseases_conditionals.osteoporosis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_kidney_disease(self):
    result = self.client.health.health_medicine.diseases_conditionals.kidney_disease()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_heartburn(self):
    result = self.client.health.health_medicine.diseases_conditionals.heartburn()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_diseases_and_conditions(self):
    result = self.client.health.health_medicine.diseases_conditionals.diseases_and_conditions()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
