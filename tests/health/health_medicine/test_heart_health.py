import unittest

from scientpyfic.client import ScientPyClient

class TestHeartHealth(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_vioxx(self):
    result = self.client.health.health_medicine.diseases_conditionals.heart_health.vioxx()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cholesterol(self):
    result = self.client.health.health_medicine.diseases_conditionals.heart_health.cholesterol()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_stroke_prevention(self):
    result = self.client.health.health_medicine.diseases_conditionals.heart_health.stroke_prevention()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
