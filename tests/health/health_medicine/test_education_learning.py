import unittest

from scientpyfic.client import ScientPyClient

class TestEducationLearning(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_public_health_education(self):
    result = self.client.health.health_medicine.education_learning.public_health_education()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_medical_education_training(self):
    result = self.client.health.health_medicine.education_learning.medical_education_and_training()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_patient_education_counseling(self):
    result = self.client.health.health_medicine.education_learning.patient_education_and_counseling()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

