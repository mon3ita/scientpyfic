import unittest

from scientpyfic.client import ScientPyClient

class TestMedicalTopics(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_foot_health(self):
    result = self.client.health.health_medicine.medical_topics.foot_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_joint_health(self):
    result = self.client.health.health_medicine.medical_topics.joint_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_gynecology(self):
    result = self.client.health.health_medicine.medical_topics.gynecology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_stem_cells(self):
    result = self.client.health.health_medicine.medical_topics.stem_cells()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_alternative_medicine(self):
    result = self.client.health.health_medicine.medical_topics.alternative_medicine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_nervous_system(self):
    result = self.client.health.health_medicine.medical_topics.nervous_system()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_controlled_substances(self):
    result = self.client.health.health_medicine.medical_topics.controlled_substances()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_epigenetics(self):
    result = self.client.health.health_medicine.medical_topics.epigenetics()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_genes(self):
    result = self.client.health.health_medicine.medical_topics.genes()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_immune_system(self):
    result = self.client.health.health_medicine.medical_topics.immune_system()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_chronic_illness(self):
    result = self.client.health.health_medicine.medical_topics.chronic_illness()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_disability(self):
    result = self.client.health.health_medicine.medical_topics.disability()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_foodborne_illness(self):
    result = self.client.health.health_medicine.medical_topics.foodborne_illness()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_pain_control(self):
    result = self.client.health.health_medicine.medical_topics.pain_control()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_menopause(self):
    result = self.client.health.health_medicine.medical_topics.menopause()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_eye_care(self):
    result = self.client.health.health_medicine.medical_topics.eye_care()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_gene_therapy(self):
    result = self.client.health.health_medicine.medical_topics.gene_therapy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_food_additives(self):
    result = self.client.health.health_medicine.medical_topics.food_additives()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_accident_and_trauma(self):
    result = self.client.health.health_medicine.medical_topics.accident_and_trauma()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_dentistry(self):
    result = self.client.health.health_medicine.medical_topics.dentistry()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_folic_acid(self):
    result = self.client.health.health_medicine.medical_topics.folic_acid()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_forensics(self):
    result = self.client.health.health_medicine.medical_topics.forensics()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_bone_and_spine(self):
    result = self.client.health.health_medicine.medical_topics.bone_and_spine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_birth_control(self):
    result = self.client.health.health_medicine.medical_topics.birth_control()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_personalized_medicine(self):
    result = self.client.health.health_medicine.medical_topics.personalized_medicine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_medical_imaging(self):
    result = self.client.health.health_medicine.medical_topics.medical_imaging()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_wounds_and_healing(self):
    result = self.client.health.health_medicine.medical_topics.wounds_and_healing()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_health_policy(self):
    result = self.client.health.health_medicine.medical_topics.health_policy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_human_biology(self):
    result = self.client.health.health_medicine.medical_topics.human_biology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_psychological_research(self):
    result = self.client.health.health_medicine.medical_topics.psychological_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_dietary_supplements_and_minerals(self):
    result = self.client.health.health_medicine.medical_topics.dietary_supplements_and_minerals()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_smoking(self):
    result = self.client.health.health_medicine.medical_topics.smoking()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_vaccines(self):
    result = self.client.health.health_medicine.medical_topics.vaccines()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_viruses(self):
    result = self.client.health.health_medicine.medical_topics.viruses()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
