import unittest

from scientpyfic.client import ScientPyClient

class TestColdFlu(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_influenza(self):
    result = self.client.health.health_medicine.diseases_conditionals.cold_flu.influenza()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cold_and_flu(self):
    result = self.client.health.health_medicine.diseases_conditionals.cold_flu.cold_and_flu()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_bird_flu(self):
    result = self.client.health.health_medicine.diseases_conditionals.cold_flu.bird_flu()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
