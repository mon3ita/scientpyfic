import unittest

from scientpyfic.client import ScientPyClient

class TestHealthMedicine(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_health_medicine(self):
    result = self.client.health.health_medicine.health_and_medicine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
