import unittest

from scientpyfic.client import ScientPyClient

class TestCancer(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_cervical_cancer(self):
    result = self.client.health.health_medicine.diseases_conditionals.cancer.cervical_cancer()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_bladder_cancer(self):
    result = self.client.health.health_medicine.diseases_conditionals.cancer.cervical_cancer()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_multiple_myeloma(self):
    result = self.client.health.health_medicine.diseases_conditionals.cancer.multiple_myeloma()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_brain_tumor(self):
    result = self.client.health.health_medicine.diseases_conditionals.cancer.brain_tumor()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_colon_cancer(self):
    result = self.client.health.health_medicine.diseases_conditionals.cancer.colon_cancer()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_breast_cancer(self):
    result = self.client.health.health_medicine.diseases_conditionals.cancer.breast_cancer()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ovarian_cancer(self):
    result = self.client.health.health_medicine.diseases_conditionals.cancer.ovarian_cancer()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_lung_cancer(self):
    result = self.client.health.health_medicine.diseases_conditionals.cancer.lung_cancer()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
