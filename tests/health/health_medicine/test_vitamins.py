import unittest

from scientpyfic.client import ScientPyClient

class TestVitamins(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_vitamin(self):
    result = self.client.health.health_medicine.medical_topics.vitamins.vitamin()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_vitamin_a(self):
    result = self.client.health.health_medicine.medical_topics.vitamins.vitamin_a()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_vitamin_b(self):
    result = self.client.health.health_medicine.medical_topics.vitamins.vitamin_b()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_vitamin_c(self):
    result = self.client.health.health_medicine.medical_topics.vitamins.vitamin_c()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_vitamin_d(self):
    result = self.client.health.health_medicine.medical_topics.vitamins.vitamin_d()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_vitamin_e(self):
    result = self.client.health.health_medicine.medical_topics.vitamins.vitamin_e()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
