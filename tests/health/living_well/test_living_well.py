import unittest

from scientpyfic.client import ScientPyClient

class TestLivingWell(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_living_well(self):
    result = self.client.health.living_well.living_well()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_healthy_aging(self):
    result = self.client.health.living_well.healthy_aging()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mens_health(self):
    result = self.client.health.living_well.mens_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_nutrition(self):
    result = self.client.health.living_well.nutrition()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_teen_health(self):
    result = self.client.health.living_well.teen_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_child_development(self):
    result = self.client.health.living_well.child_development()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_pregnancy_and_childbirth(self):
    result = self.client.health.living_well.pregnancy_and_childbirth()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_stress(self):
    result = self.client.health.living_well.stress()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_elder_care(self):
    result = self.client.health.living_well.elder_care()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_consumer_behavior(self):
    result = self.client.health.living_well.consumer_behavior()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fitness(self):
    result = self.client.health.living_well.fitness()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_spirituality(self):
    result = self.client.health.living_well.spirituality()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_womens_health(self):
    result = self.client.health.living_well.womens_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_skin_care(self):
    result = self.client.health.living_well.skin_care()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_todays_healthcare(self):
    result = self.client.health.living_well.todays_healthcare()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_breastfeeding(self):
    result = self.client.health.living_well.breastfeeding()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sports_medicine(self):
    result = self.client.health.living_well.sports_medicine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sexual_health(self):
    result = self.client.health.living_well.sexual_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_dieting_and_weihgt_control(self):
    result = self.client.health.living_well.dieting_and_weihgt_control()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fertility(self):
    result = self.client.health.living_well.fertility()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_cosmetic_surgery(self):
    result = self.client.health.living_well.cosmetic_surgery()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_vegetarian(self):
    result = self.client.health.living_well.vegetarian()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_nutrition_research(self):
    result = self.client.health.living_well.nutrition_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_staying_healthy(self):
    result = self.client.health.living_well.staying_healthy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_childrens_health(self):
    result = self.client.health.living_well.childrens_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_behavior(self):
    result = self.client.health.living_well.behavior()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_caregiving(self):
    result = self.client.health.living_well.caregiving()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_infants_health(self):
    result = self.client.health.living_well.infants_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_parenting(self):
    result = self.client.health.living_well.parenting()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_anger_management(self):
    result = self.client.health.living_well.anger_management()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_diet_and_weight_loss(self):
    result = self.client.health.living_well.diet_and_weight_loss()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_gender_difference(self):
    result = self.client.health.living_well.gender_difference()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_racial_issues(self):
    result = self.client.health.living_well.racial_issues()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_relationships(self):
    result = self.client.health.living_well.relationships()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
