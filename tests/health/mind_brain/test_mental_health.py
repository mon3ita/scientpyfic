import unittest

from scientpyfic.client import ScientPyClient

class TestMentalHealth(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_mental_health(self):
    result = self.client.health.mind_brain.mental_health.mental_health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_alcoholism(self):
    result = self.client.health.mind_brain.mental_health.alcoholism()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_anxiety(self):
    result = self.client.health.mind_brain.mental_health.anxiety()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_eating_disorders(self):
    result = self.client.health.mind_brain.mental_health.eating_disorders()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_smoking_addiction(self):
    result = self.client.health.mind_brain.mental_health.smoking_addiction()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_addiction(self):
    result = self.client.health.mind_brain.mental_health.addiction()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)