import unittest

from scientpyfic.client import ScientPyClient

class TestDisordersAndSyndromes(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_dyslexia(self):
    result = self.client.health.mind_brain.education_learning.dyslexia()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_educational_psychology(self):
    result = self.client.health.mind_brain.education_learning.educational_psychology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_k_12_education(self):
    result = self.client.health.mind_brain.education_learning.k_12_education()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_numeracy(self):
    result = self.client.health.mind_brain.education_learning.numeracy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_brain_computer_interfaces(self):
    result = self.client.health.mind_brain.education_learning.brain_computer_interfaces()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_learning_disorders(self):
    result = self.client.health.mind_brain.education_learning.learning_disorders()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_language_acquisition(self):
    result = self.client.health.mind_brain.education_learning.language_acquisition()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_music(self):
    result = self.client.health.mind_brain.education_learning.music()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_creativity(self):
    result = self.client.health.mind_brain.education_learning.creativity()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_infant_and_preschool_learning(self):
    result = self.client.health.mind_brain.education_learning.infant_and_preschool_learning()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_literacy(self):
    result = self.client.health.mind_brain.education_learning.literacy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_intelligence(self):
    result = self.client.health.mind_brain.education_learning.intelligence()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
