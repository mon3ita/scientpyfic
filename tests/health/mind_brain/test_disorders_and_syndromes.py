import unittest

from scientpyfic.client import ScientPyClient

class TestDisordersAndSyndromes(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_add_adhd(self):
    result = self.client.health.mind_brain.disorders_syndromes.add_and_adhd()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_alzheimers(self):
    result = self.client.health.mind_brain.disorders_syndromes.alzheimers()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_dementia(self):
    result = self.client.health.mind_brain.disorders_syndromes.dementia()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_schizophrenia(self):
    result = self.client.health.mind_brain.disorders_syndromes.schizophrenia()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_borderline_personality_disorder(self):
    result = self.client.health.mind_brain.disorders_syndromes.borderline_personality_disorder()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_bipolar_disorder(self):
    result = self.client.health.mind_brain.disorders_syndromes.bipolar_disorder()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_depression(self):
    result = self.client.health.mind_brain.disorders_syndromes.depression()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_huntingtons_disease(self):
    result = self.client.health.mind_brain.disorders_syndromes.huntingtons_disease()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_insomnia(self):
    result = self.client.health.mind_brain.disorders_syndromes.insomnia()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_parkinsons(self):
    result = self.client.health.mind_brain.disorders_syndromes.parkinsons()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_stroke(self):
    result = self.client.health.mind_brain.disorders_syndromes.stroke()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_headaches(self):
    result = self.client.health.mind_brain.disorders_syndromes.headaches()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_multiple_sclerosis(self):
    result = self.client.health.mind_brain.disorders_syndromes.multiple_sclerosis()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_tinnitus(self):
    result = self.client.health.mind_brain.disorders_syndromes.tinnitus()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_autism(self):
    result = self.client.health.mind_brain.disorders_syndromes.autism()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_hearing_impairment(self):
    result = self.client.health.mind_brain.disorders_syndromes.hearing_impairment()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_epilepsy(self):
    result = self.client.health.mind_brain.disorders_syndromes.epilepsy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_obstructive_sleep_apnea(self):
    result = self.client.health.mind_brain.disorders_syndromes.obstructive_sleep_apnea()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_sleep_disorders(self):
    result = self.client.health.mind_brain.disorders_syndromes.sleep_disorders()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ptsd(self):
    result = self.client.health.mind_brain.disorders_syndromes.ptsd()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mad_cow_disease(self):
    result = self.client.health.mind_brain.disorders_syndromes.mad_cow_disease()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_disorders_and_syndromes(self):
    result = self.client.health.mind_brain.disorders_syndromes.disorders_and_syndromes()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_disorders_and_syndromes(self):
    result = self.client.health.mind_brain.disorders_syndromes.brain_injury()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
