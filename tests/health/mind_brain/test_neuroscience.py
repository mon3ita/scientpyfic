import unittest

from scientpyfic.client import ScientPyClient

class TestNeuroscience(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_memory(self):
    result = self.client.health.mind_brain.neuroscience.memory()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_perception(self):
    result = self.client.health.mind_brain.neuroscience.perception()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_neuroscience(self):
    result = self.client.health.mind_brain.neuroscience.neuroscience()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)