import unittest

from scientpyfic.client import ScientPyClient

class TestPscychology(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_psychology(self):
    result = self.client.health.mind_brain.psychology.psychology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_child_psychology(self):
    result = self.client.health.mind_brain.psychology.child_psychology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_social_psychology(self):
    result = self.client.health.mind_brain.psychology.social_psychology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
