import unittest

from scientpyfic.client import ScientPyClient

class TestLivingWell(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_child_development(self):
    result = self.client.health.mind_brain.living_well.child_development()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_stress(self):
    result = self.client.health.mind_brain.living_well.stress()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_consumer_behavior(self):
    result = self.client.health.mind_brain.living_well.consumer_behavior()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_spirituality(self):
    result = self.client.health.mind_brain.living_well.spirituality()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_dieting_and_weight_control(self):
    result = self.client.health.mind_brain.living_well.dieting_and_weight_control()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_nutrition_research(self):
    result = self.client.health.mind_brain.living_well.nutrition_research()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_behavior(self):
    result = self.client.health.mind_brain.living_well.behavior()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_caregiving(self):
    result = self.client.health.mind_brain.living_well.caregiving()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_parenting(self):
    result = self.client.health.mind_brain.living_well.parenting()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_anger_management(self):
    result = self.client.health.mind_brain.living_well.anger_management()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_gender_difference(self):
    result = self.client.health.mind_brain.living_well.gender_difference()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_racial_issues(self):
    result = self.client.health.mind_brain.living_well.racial_issues()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_relationships(self):
    result = self.client.health.mind_brain.living_well.relationships()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
