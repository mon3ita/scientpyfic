import unittest

from scientpyfic.client import ScientPyClient

class TestIllegalDrugs(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_cocaine(self):
    result = self.client.health.mind_brain.illegal_drugs.cocaine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_steroids(self):
    result = self.client.health.mind_brain.illegal_drugs.steroids()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_marijuana(self):
    result = self.client.health.mind_brain.illegal_drugs.marijuana()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_opium(self):
    result = self.client.health.mind_brain.illegal_drugs.opium()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_crystal_meth(self):
    result = self.client.health.mind_brain.illegal_drugs.crystal_meth()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_illegal_drugs(self):
    result = self.client.health.mind_brain.illegal_drugs.illegal_drugs()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_illegal_psychedelic_drugs(self):
    result = self.client.health.mind_brain.illegal_drugs.psychedelic_drugs()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_ecstasy(self):
    result = self.client.health.mind_brain.illegal_drugs.ecstasy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
