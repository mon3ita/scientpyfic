import unittest

from scientpyfic.client import ScientPyClient

class TestQuirks(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  import unittest

  def test_strange_offbeat(self):
    result = self.client.quirky.strange_offbeat()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_living_well(self):
    result = self.client.quirky.living_well()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_human_quirks(self):
    result = self.client.quirky.human_quirks()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_health_medicine(self):
    result = self.client.quirky.health_medicine()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_mind_brain(self):
    result = self.client.quirky.mind_brain()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_bizarre_things(self):
    result = self.client.quirky.bizarre_things()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_space_time(self):
    result = self.client.quirky.space_time()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_matter_energy(self):
    result = self.client.quirky.matter_energy()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_computers_math(self):
    result = self.client.quirky.computers_math()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_odd_creatures(self):
    result = self.client.quirky.odd_creatures()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_plants_animals(self):
    result = self.client.quirky.plants_animals()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_earth_climate(self):
    result = self.client.quirky.earth_climate()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_fossils_ruins(self):
    result = self.client.quirky.fossils_ruins()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_weird_world(self):
    result = self.client.quirky.weird_world()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_science_society(self):
    result = self.client.quirky.science_society()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_business_industry(self):
    result = self.client.quirky.business_industry()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_education_learning(self):
    result = self.client.quirky.education_learning()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
