import unittest

from scientpyfic.client import ScientPyClient


class TestEngineering(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_sports_science(self):
        result = self.client.technology.matter_energy.engineering.sports_science()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_virtual_environment(self):
        result = self.client.technology.matter_energy.engineering.virtual_environment()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_weapons_technology(self):
        result = self.client.technology.matter_energy.engineering.weapons_technology()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_civil_engineering(self):
        result = self.client.technology.matter_energy.engineering.civil_engineering()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_forensic_research(self):
        result = self.client.technology.matter_energy.engineering.forensic_research()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_microarrays(self):
        result = self.client.technology.matter_energy.engineering.microarrays()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_construction(self):
        result = self.client.technology.matter_energy.engineering.construction()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_wearable_technology(self):
        result = self.client.technology.matter_energy.engineering.wearable_technology()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_aviation(self):
        result = self.client.technology.matter_energy.engineering.aviation()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_detectors(self):
        result = self.client.technology.matter_energy.engineering.detectors()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_spintronics(self):
        result = self.client.technology.matter_energy.engineering.spintronics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_medical_technology(self):
        result = self.client.technology.matter_energy.engineering.medical_technology()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_biometric(self):
        result = self.client.technology.matter_energy.engineering.biometric()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_graphene(self):
        result = self.client.technology.matter_energy.engineering.graphene()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_robotics_research(self):
        result = self.client.technology.matter_energy.engineering.robotics_research()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_nanotechnology(self):
        result = self.client.technology.matter_energy.engineering.nanotechnology()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_three_d_printing(self):
        result = self.client.technology.matter_energy.engineering.three_d_printing()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_electronics(self):
        result = self.client.technology.matter_energy.engineering.electronics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_vehicles(self):
        result = self.client.technology.matter_energy.engineering.vehicles()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_engineering(self):
        result = self.client.technology.matter_energy.engineering.engineering()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_materials_science(self):
        result = self.client.technology.matter_energy.engineering.materials_science()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_transportation_science(self):
        result = (
            self.client.technology.matter_energy.engineering.transportation_science()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
