import unittest

from scientpyfic.client import ScientPyClient


class TestMatterEnergy(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_matter_energy(self):
        result = self.client.technology.matter_energy.matter_energy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
