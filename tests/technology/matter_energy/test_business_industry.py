import unittest

from scientpyfic.client import ScientPyClient


class TestBusinessIndustry(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_textiles_and_clothing(self):
        result = (
            self.client.technology.matter_energy.business_industry.textiles_and_clothing()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_engineering_and_construction(self):
        result = (
            self.client.technology.matter_energy.business_industry.engineering_and_construction()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_energy_and_resources(self):
        result = (
            self.client.technology.matter_energy.business_industry.energy_and_resources()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_telecommunications(self):
        result = (
            self.client.technology.matter_energy.business_industry.telecommunications()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_automotive_and_transportation(self):
        result = (
            self.client.technology.matter_energy.business_industry.automotive_and_transportation()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_aerospace(self):
        result = self.client.technology.matter_energy.business_industry.aerospace()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_consumer_electronics(self):
        result = (
            self.client.technology.matter_energy.business_industry.consumer_electronics()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
