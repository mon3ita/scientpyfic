import unittest

from scientpyfic.client import ScientPyClient


class TestChemistry(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_biochemistry(self):
        result = self.client.technology.matter_energy.chemistry.biochemistry()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_inorganic_chemistry(self):
        result = self.client.technology.matter_energy.chemistry.inorganic_chemistry()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_organic_chemistry(self):
        result = self.client.technology.matter_energy.chemistry.organic_chemistry()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_chemistry(self):
        result = self.client.technology.matter_energy.chemistry.chemistry()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_thermodynamics(self):
        result = self.client.technology.matter_energy.chemistry.thermodynamics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
