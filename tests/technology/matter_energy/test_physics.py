import unittest

from scientpyfic.client import ScientPyClient


class TestMatterEnergy(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_albert_einstein(self):
        result = self.client.technology.matter_energy.physics.albert_einstein()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_quantum_physics(self):
        result = self.client.technology.matter_energy.physics.quantum_physics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_nature_of_water(self):
        result = self.client.technology.matter_energy.physics.nature_of_water()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_quantum_computing(self):
        result = self.client.technology.matter_energy.physics.quantum_computing()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_ultrasound(self):
        result = self.client.technology.matter_energy.physics.ultrasound()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_acoustics(self):
        result = self.client.technology.matter_energy.physics.acoustics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_optics(self):
        result = self.client.technology.matter_energy.physics.optics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_physics(self):
        result = self.client.technology.matter_energy.physics.physics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
