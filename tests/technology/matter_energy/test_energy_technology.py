import unittest

from scientpyfic.client import ScientPyClient


class TestEnergyTechnology(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_petroleum(self):
        result = self.client.technology.matter_energy.energy_technology.petroleum()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_energy_policy(self):
        result = self.client.technology.matter_energy.energy_technology.energy_policy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_wind_energy(self):
        result = self.client.technology.matter_energy.energy_technology.wind_energy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_batteries(self):
        result = self.client.technology.matter_energy.energy_technology.batteries()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_nuclear_energy(self):
        result = self.client.technology.matter_energy.energy_technology.nuclear_energy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_alternative_fuels(self):
        result = (
            self.client.technology.matter_energy.energy_technology.alternative_fuels()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_fossil_fuels(self):
        result = self.client.technology.matter_energy.energy_technology.fossil_fuels()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_solar_energy(self):
        result = self.client.technology.matter_energy.energy_technology.solar_energy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
