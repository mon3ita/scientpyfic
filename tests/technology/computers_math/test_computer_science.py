import unittest

from scientpyfic.client import ScientPyClient


class TestComputerScience(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_computational_biology(self):
        result = (
            self.client.technology.computers_math.computer_science.computational_biology()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_computer_graphics(self):
        result = (
            self.client.technology.computers_math.computer_science.computer_graphics()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_computer_modeling(self):
        result = (
            self.client.technology.computers_math.computer_science.computer_modeling()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_computer_science(self):
        result = (
            self.client.technology.computers_math.computer_science.computer_science()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_encryption(self):
        result = self.client.technology.computers_math.computer_science.encryption()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_mobile_computing(self):
        result = (
            self.client.technology.computers_math.computer_science.mobile_computing()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_artificial_intelligence(self):
        result = (
            self.client.technology.computers_math.computer_science.artificial_intelligence()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_video_games(self):
        result = self.client.technology.computers_math.computer_science.video_games()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_virtual_reality(self):
        result = (
            self.client.technology.computers_math.computer_science.virtual_reality()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_distributed_computing(self):
        result = (
            self.client.technology.computers_math.computer_science.distributed_computing()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_computer_programming(self):
        result = (
            self.client.technology.computers_math.computer_science.computer_programming()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_quantum_computers(self):
        result = (
            self.client.technology.computers_math.computer_science.quantum_computers()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_spintronics_research(self):
        result = (
            self.client.technology.computers_math.computer_science.spintronics_research()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_wifi(self):
        result = self.client.technology.computers_math.computer_science.wifi()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_information_technology(self):
        result = (
            self.client.technology.computers_math.computer_science.information_technology()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_robotics(self):
        result = self.client.technology.computers_math.computer_science.robotics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_hacking(self):
        result = self.client.technology.computers_math.computer_science.hacking()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_internet(self):
        result = self.client.technology.computers_math.computer_science.internet()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_software(self):
        result = self.client.technology.computers_math.computer_science.software()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_communications(self):
        result = self.client.technology.computers_math.computer_science.communications()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_photography(self):
        result = self.client.technology.computers_math.computer_science.photography()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
