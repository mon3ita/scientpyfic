import unittest

from scientpyfic.client import ScientPyClient


class TestMath(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_math_puzzles(self):
        result = self.client.technology.computers_math.mathematics.math_puzzles()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_mathematical_modeling(self):
        result = (
            self.client.technology.computers_math.mathematics.mathematical_modeling()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_mathematics(self):
        result = self.client.technology.computers_math.mathematics.mathematics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_statistics(self):
        result = self.client.technology.computers_math.mathematics.statistics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
