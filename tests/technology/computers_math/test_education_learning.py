import unittest

from scientpyfic.client import ScientPyClient


class TestEducationaLearning(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_neural_intefaces(self):
        result = (
            self.client.technology.computers_math.education_learning.neural_intefaces()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_educational_technology(self):
        result = (
            self.client.technology.computers_math.education_learning.educational_technology()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
