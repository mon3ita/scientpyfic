import unittest

from scientpyfic.client import ScientPyClient


class TestComputersMath(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_computers_math(self):
        result = self.client.technology.computers_math.computers_and_math()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
