import unittest

from scientpyfic.client import ScientPyClient


class TestBusinessIndustry(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_markets_and_finance(self):
        result = (
            self.client.technology.computers_math.business_industry.markets_and_finance()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_computers_and_internet(self):
        result = (
            self.client.technology.computers_math.business_industry.computers_and_internet()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
