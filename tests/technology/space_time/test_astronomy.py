import unittest

from scientpyfic.client import ScientPyClient


class TestAstronomy(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_black_holes(self):
        result = self.client.technology.space_time.astronomy.black_holes()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_stars(self):
        result = self.client.technology.space_time.astronomy.stars()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_cosmic_rays(self):
        result = self.client.technology.space_time.astronomy.cosmic_rays()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_nebulae(self):
        result = self.client.technology.space_time.astronomy.nebulae()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_astronomy(self):
        result = self.client.technology.space_time.astronomy.astronomy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_galaxies(self):
        result = self.client.technology.space_time.astronomy.galaxies()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_extrasolar_planets(self):
        result = self.client.technology.space_time.astronomy.extrasolar_planets()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_dark_matter(self):
        result = self.client.technology.space_time.astronomy.dark_matter()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
