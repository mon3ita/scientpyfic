import unittest

from scientpyfic.client import ScientPyClient


class TestSpaceTime(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_space_time(self):
        result = self.client.technology.space_time.space_time()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
