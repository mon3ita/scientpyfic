import unittest

from scientpyfic.client import ScientPyClient


class TestCosmology(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_big_bang(self):
        result = self.client.technology.space_time.solar_system.solar_system()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_kuiper_belt(self):
        result = self.client.technology.space_time.solar_system.kuiper_belt()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_moon(self):
        result = self.client.technology.space_time.solar_system.moon()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_neptune(self):
        result = self.client.technology.space_time.solar_system.neptune()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_asteroids_comets_meteors(self):
        result = (
            self.client.technology.space_time.solar_system.asteroids_comets_meteors()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_venus(self):
        result = self.client.technology.space_time.solar_system.venus()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_saturn(self):
        result = self.client.technology.space_time.solar_system.saturn()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_mars(self):
        result = self.client.technology.space_time.solar_system.mars()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_mercury(self):
        result = self.client.technology.space_time.solar_system.mercury()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_northern_lights(self):
        result = self.client.technology.space_time.solar_system.northern_lights()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_sun(self):
        result = self.client.technology.space_time.solar_system.sun()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_uranus(self):
        result = self.client.technology.space_time.solar_system.uranus()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_jupiter(self):
        result = self.client.technology.space_time.solar_system.jupiter()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_solar_flare(self):
        result = self.client.technology.space_time.solar_system.solar_flare()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_pluto(self):
        result = self.client.technology.space_time.solar_system.pluto()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_eris(self):
        result = self.client.technology.space_time.solar_system.pluto()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
