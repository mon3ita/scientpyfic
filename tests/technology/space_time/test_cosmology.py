import unittest

from scientpyfic.client import ScientPyClient


class TestCosmology(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_big_bang(self):
        result = self.client.technology.space_time.cosmology.big_bang()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_cosmology(self):
        result = self.client.technology.space_time.cosmology.cosmology()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_astrophysics(self):
        result = self.client.technology.space_time.cosmology.astrophysics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
