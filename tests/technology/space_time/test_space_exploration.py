import unittest

from scientpyfic.client import ScientPyClient


class TestSpaceExploration(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_space_missions(self):
        result = self.client.technology.space_time.space_exploration.space_missions()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_space_station(self):
        result = self.client.technology.space_time.space_exploration.space_station()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_space_telescopes(self):
        result = self.client.technology.space_time.space_exploration.space_telescopes()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_esa(self):
        result = self.client.technology.space_time.space_exploration.esa()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_nasa(self):
        result = self.client.technology.space_time.space_exploration.nasa()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_space_exploration(self):
        result = self.client.technology.space_time.space_exploration.space_exploration()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_satellites(self):
        result = self.client.technology.space_time.space_exploration.satellites()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_space_probes(self):
        result = self.client.technology.space_time.space_exploration.space_probes()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
