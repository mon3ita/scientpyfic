import unittest

from scientpyfic.client import ScientPyClient


class TestScienceSociety(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_science_society(self):
        result = self.client.society.science_society.science_society.neural_interfaces()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
