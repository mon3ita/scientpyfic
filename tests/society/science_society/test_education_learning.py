import unittest

from scientpyfic.client import ScientPyClient


class TestEducationLearning(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_education_and_employment(self):
        result = (
            self.client.society.science_society.education_learning.education_and_employment()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_stem_education(self):
        result = self.client.society.science_society.education_learning.stem_education()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_poverty_and_learning(self):
        result = (
            self.client.society.science_society.education_learning.poverty_and_learning()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
