import unittest

from scientpyfic.client import ScientPyClient


class TestCulture(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_religion(self):
        result = self.client.society.science_society.culture.religion()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_sports(self):
        result = self.client.society.science_society.culture.sports()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_arts_and_culture(self):
        result = self.client.society.science_society.culture.arts_and_culture()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
