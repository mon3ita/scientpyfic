import unittest

from scientpyfic.client import ScientPyClient


class TestSciencePolicy(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_energy_issues(self):
        result = self.client.society.science_society.science_policy.energy_issues()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_ocean_policy(self):
        result = self.client.society.science_society.science_policy.ocean_policy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_scientific_conduct(self):
        result = self.client.society.science_society.science_policy.scientific_conduct()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_space_policy(self):
        result = self.client.society.science_society.science_policy.space_policy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_government_regulation(self):
        result = (
            self.client.society.science_society.science_policy.government_regulation()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_public_health(self):
        result = self.client.society.science_society.science_policy.public_health()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_privacy_issues(self):
        result = self.client.society.science_society.science_policy.privacy_issues()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_environmental_policies(self):
        result = (
            self.client.society.science_society.science_policy.environmental_policies()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_funding_policy(self):
        result = self.client.society.science_society.science_policy.funding_policy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
