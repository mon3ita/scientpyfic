import unittest

from scientpyfic.client import ScientPyClient


class TestSocialIssues(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_surveillance(self):
        result = self.client.society.science_society.social_issues.surveillance()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_transportation_issues(self):
        result = (
            self.client.society.science_society.social_issues.transportation_issues()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_resource_shortage(self):
        result = self.client.society.science_society.social_issues.resource_shortage()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_urbanization(self):
        result = self.client.society.science_society.social_issues.urbanization()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_racial_disparity(self):
        result = self.client.society.science_society.social_issues.racial_disparity()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_world_development(self):
        result = self.client.society.science_society.social_issues.world_development()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_conflict(self):
        result = self.client.society.science_society.social_issues.conflict()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_bioethics(self):
        result = self.client.society.science_society.social_issues.bioethics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_economics(self):
        result = self.client.society.science_society.social_issues.economics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_ethics(self):
        result = self.client.society.science_society.social_issues.ethics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_disaster_plan(self):
        result = self.client.society.science_society.social_issues.disaster_plan()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_justice(self):
        result = self.client.society.science_society.social_issues.justice()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_social_issues(self):
        result = self.client.society.science_society.social_issues.social_issues()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_consumerism(self):
        result = self.client.society.science_society.social_issues.consumerism()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_political_science(self):
        result = self.client.society.science_society.social_issues.political_science()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_popular_culture(self):
        result = self.client.society.science_society.social_issues.popular_culture()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_land_management(self):
        result = self.client.society.science_society.social_issues.land_management()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
