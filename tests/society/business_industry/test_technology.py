import unittest

from scientpyfic.client import ScientPyClient


class TestTechnology(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_markets_and_finance(self):
        result = self.client.society.business_industry.technology.markets_and_finance()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_textiles_and_clothing(self):
        result = (
            self.client.society.business_industry.technology.textiles_and_clothing()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_engineerig_and_construction(self):
        result = (
            self.client.society.business_industry.technology.engineerig_and_construction()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_energy_and_resources(self):
        result = self.client.society.business_industry.technology.energy_and_resources()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_telecommunications(self):
        result = self.client.society.business_industry.technology.telecommunications()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_automotive_and_transportation(self):
        result = (
            self.client.society.business_industry.technology.automotive_and_transportation()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_computers_and_internet(self):
        result = (
            self.client.society.business_industry.technology.computers_and_internet()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_aerospace(self):
        result = self.client.society.business_industry.technology.aerospace()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_consumer_electronics(self):
        result = self.client.society.business_industry.technology.consumer_electronics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
