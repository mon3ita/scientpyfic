import unittest

from scientpyfic.client import ScientPyClient


class TestHealth(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_workplace_health(self):
        result = self.client.society.business_industry.health.workplace_health()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_pharmaceuticals(self):
        result = self.client.society.business_industry.health.pharmaceuticals()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_cosmetics(self):
        result = self.client.society.business_industry.health.cosmetics()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_medical_devices(self):
        result = self.client.society.business_industry.health.medical_devices()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
