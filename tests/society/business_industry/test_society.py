import unittest

from scientpyfic.client import ScientPyClient


class TestSociety(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_media_and_entertainment(self):
        result = self.client.society.business_industry.society.media_and_entertainment()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_retail_and_services(self):
        result = self.client.society.business_industry.society.retail_and_services()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_industrial_relations(self):
        result = self.client.society.business_industry.society.industrial_relations()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_security_and_defense(self):
        result = self.client.society.business_industry.society.security_and_defense()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_travel_and_recreation(self):
        result = self.client.society.business_industry.society.travel_and_recreation()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
