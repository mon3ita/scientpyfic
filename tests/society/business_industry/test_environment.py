import unittest

from scientpyfic.client import ScientPyClient


class TestEnvrionment(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_biotechnology_and_bioengineering(self):
        result = (
            self.client.society.business_industry.environment.biotechnology_and_bioengineering()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_food_and_agriculture(self):
        result = (
            self.client.society.business_industry.environment.food_and_agriculture()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_renewable_energy(self):
        result = self.client.society.business_industry.environment.renewable_energy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_geoengineering(self):
        result = self.client.society.business_industry.environment.geoengineering()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_recycling_and_waste(self):
        result = self.client.society.business_industry.environment.recycling_and_waste()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_mining(self):
        result = self.client.society.business_industry.environment.mining()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_mining(self):
        result = self.client.society.business_industry.environment.mining()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
