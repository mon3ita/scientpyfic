import unittest

from scientpyfic.client import ScientPyClient


class TestBusinessIndustry(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_business_industry(self):
        result = self.client.society.business_industry.business_industry()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
