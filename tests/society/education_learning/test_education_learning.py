import unittest

from scientpyfic.client import ScientPyClient


class TestEducationLearning(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_education_learning(self):
        result = self.client.society.education_learning.education_and_learning()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
