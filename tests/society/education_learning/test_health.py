import unittest

from scientpyfic.client import ScientPyClient


class TestHealth(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_dyslexia(self):
        result = self.client.society.education_learning.health.dyslexia()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_educational_psychology(self):
        result = self.client.society.education_learning.health.educational_psychology()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_k_12_education(self):
        result = self.client.society.education_learning.health.k_12_education()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_numeracy(self):
        result = self.client.society.education_learning.health.numeracy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_public_health_education(self):
        result = self.client.society.education_learning.health.public_health_education()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_medical_education_and_training(self):
        result = (
            self.client.society.education_learning.health.medical_education_and_training()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_brain_computer_interfaces(self):
        result = (
            self.client.society.education_learning.health.medical_education_and_training()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_learning_disorders(self):
        result = self.client.society.education_learning.health.learning_disorders()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_language_acquisition(self):
        result = self.client.society.education_learning.health.language_acquisition()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_music(self):
        result = self.client.society.education_learning.health.music()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_patient_education_and_counseling(self):
        result = (
            self.client.society.education_learning.health.patient_education_and_counseling()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_creativity(self):
        result = self.client.society.education_learning.health.creativity()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_infant_and_preschool_learning(self):
        result = (
            self.client.society.education_learning.health.infant_and_preschool_learning()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_literacy(self):
        result = self.client.society.education_learning.health.literacy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_intelligence(self):
        result = self.client.society.education_learning.health.intelligence()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
