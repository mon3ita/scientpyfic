import unittest

from scientpyfic.client import ScientPyClient


class TestTechnology(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_neural_interfaces(self):
        result = self.client.society.education_learning.technology.neural_interfaces()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_educational_technology(self):
        result = (
            self.client.society.education_learning.technology.educational_technology()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
