import unittest

from scientpyfic.client import ScientPyClient


class TestEnvironment(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_environmental_awareness(self):
        result = (
            self.client.society.education_learning.environment.environmental_awareness()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_animal_learning_and_intelligence(self):
        result = (
            self.client.society.education_learning.environment.animal_learning_and_intelligence()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
