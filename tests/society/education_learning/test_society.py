import unittest

from scientpyfic.client import ScientPyClient


class TestSociety(unittest.TestCase):
    def setUp(self):
        self.client = ScientPyClient()

    def test_education_and_employment(self):
        result = (
            self.client.society.education_learning.society.education_and_employment()
        )
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_stem_education(self):
        result = self.client.society.education_learning.society.stem_education()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_poverty_and_learning(self):
        result = self.client.society.education_learning.society.poverty_and_learning()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )

    def test_educational_policy(self):
        result = self.client.society.education_learning.society.educational_policy()
        self.assertTrue(
            len(result) > 0
            and result[0].title is not None
            and result[0].pub_date is not None
            and result[0].description is not None
        )
