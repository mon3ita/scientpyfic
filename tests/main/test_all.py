import unittest

from scientpyfic.client import ScientPyClient

class TestAll(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_all(self):
    result = self.client.all.all()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
