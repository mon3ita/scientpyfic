import unittest

from scientpyfic.client import ScientPyClient

class TestTop(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_top(self):
    result = self.client.top.top()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_science(self):
    result = self.client.top.science()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_health(self):
    result = self.client.top.health()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_technology(self):
    result = self.client.top.technology()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_environment(self):
    result = self.client.top.environment()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)

  def test_society(self):
    result = self.client.top.society()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)