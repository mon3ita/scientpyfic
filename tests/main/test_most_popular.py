import unittest

from scientpyfic.client import ScientPyClient

class TestMostPopular(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_most_popular(self):
    result = self.client.most_popular.most_popular()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
