import unittest

from scientpyfic.client import ScientPyClient

class TestStraangeOffbeat(unittest.TestCase):

  def setUp(self):
    self.client = ScientPyClient()

  def test_strange_offbeat(self):
    result = self.client.strange_offbeat.strange_offbeat()
    self.assertTrue(len(result) > 0 and result[0].title is not None and result[0].pub_date is not None
      and result[0].description is not None)
